const AddAppButton = (props) => {
  const { operatingSystem } = props;
  let storeLink =
    "https://play.google.com/store/apps/details?id=org.altruist.BajajExperia&shortlink=appdk&pid=SMS&c=Marketing_Web_Reimagin";
  if (operatingSystem.toLowerCase().includes("ios")) {
    storeLink = "https://apps.apple.com/in/app/bajaj-finserv/id505454145";
  }

  return (
    <a href={storeLink} target="_blank" rel="noreferrer">
      <button>Download the app</button>
    </a>
  );
};

export default AddAppButton;
