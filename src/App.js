import { UAParser } from "ua-parser-js";
import "./App.css";
import AddAppButton from "./components/add-app-button/";

function App() {
  const parser = new UAParser();
  const Osname = parser.getOS().name;
  return (
    <div className="App">
      <div className="details">You are viewing this page from {Osname} OS.</div>
      <div className="bar">
        <AddAppButton className="button" operatingSystem={Osname} />
      </div>
    </div>
  );
}

export default App;
